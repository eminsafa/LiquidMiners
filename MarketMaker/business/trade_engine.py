from django.db import models
import datetime, time
from ..models import Investment, Order, Log, Status
from .exchange_factory import ExchangeFactory


def singleton(class_):
    instances = {}

    def get_instance(*args, **kwargs):
        if class_ not in instances:
            instances[class_] = class_(*args, **kwargs)
        return instances[class_]

    return get_instance


@singleton
class TradeEngine:

    _status = False
    _EF = ExchangeFactory()


    def __init__(self):
        self._status = False

    def check_credentials(self, exchange_name, private_key, public_key):
        return self._EF.check_credentials(exchange_name, private_key, public_key)

    def get_unprocessed_investments(self):
        time_diff = datetime.datetime.now() - datetime.timedelta(seconds=50)  # @todo make it dynamic
        unprocessed_investments = Investment.objects.filter(updated_at__lte=time_diff, status__name='ACTIVE', reward__end_date__gte=datetime.datetime.now())
        print("\033[0;35m          | Investments           " + str(len(unprocessed_investments)) + "\033[0m")
        return unprocessed_investments

    def get_orders_of_investment(self, investment):
        orders = Order.objects.filter(investment=investment, status__name='ACTIVE')
        return orders

    def get_orders_of_investments(self, investments):
        orders_of_investments = {}
        order_counter = 0
        for investment in investments:
            orders = self.get_orders_of_investment(investment)
            orders_of_investments[investment] = {'orders': []}
            for order in orders:
                order_counter += 1
                orders_of_investments[investment]['orders'].append(order)
        print("\033[0;35m          | Orders                " + str(order_counter) + "\033[0m")
        return orders_of_investments

    def investment_controller(self, investments):
        for investment in investments:
            user = investment.user
            pair = investment.reward.pair
            exchange = self._EF.get(user, investment.reward.exchange)
            orders = Order.objects.filter(investment=investment, status__name='ACTIVE')

            # CANCEL ALL ORDERS
            for order in orders:
                r = exchange.cancel_order(order)
                if not r:
                    return False

            # GET PRICE and WALLET DATA
            buy_price, sell_price = exchange.get_dynamic_price(pair.eid, investment.minimum_spread)
            if buy_price == 0.0 or sell_price == 0.0:
                Log(type='error', message='price is 0.0 -> STOPPED').save()
                return False
            balances = exchange.get_user_balance()
            crypto_amount = balances[pair.buy_side.eid]
            fiat_amount = balances[pair.sell_side.eid]
            crypto_value = crypto_amount * buy_price
            fiat_value = fiat_amount

            new_order_1 = Temp()
            new_order_2 = Temp()

            if crypto_value > (fiat_value * 2):
                new_order_1.side = 'sell'
                new_order_1.amount = crypto_amount / 2
                new_order_1.price = sell_price
                new_order_2.side = 'sell'
                new_order_2.amount = crypto_amount / 2
                new_order_2.price = sell_price
                if (new_order_2.amount + new_order_1.amount) > crypto_amount:
                    new_order_2.amount = new_order_2.amount * 0.999
            elif fiat_value > (crypto_value * 2):
                new_order_1.side = 'buy'
                new_order_1.amount = (fiat_amount / buy_price) / 2.0
                new_order_1.price = buy_price
                new_order_2.side = 'buy'
                new_order_2.amount = (fiat_amount / buy_price) / 2.0
                new_order_2.price = buy_price
                if ((new_order_2.amount*buy_price) + (new_order_1.amount*buy_price)) > fiat_amount:
                    new_order_2.amount = new_order_2.amount * 0.999
            else:
                new_order_2.side = 'buy'
                new_order_2.amount = fiat_amount / buy_price
                new_order_2.price = buy_price
                new_order_1.side = 'sell'
                new_order_1.amount = crypto_amount
                new_order_1.price = sell_price

            status = Status.objects.get(name='ACTIVE')

            # NEW ORDER 1
            eid = exchange.create_order(pair.eid, new_order_1.amount, new_order_1.price, new_order_1.side)
            if eid == 0:
                Log.on('Order Creating Error - Exchange: '+exchange, 'error', investment.user)
            else:
                Order(eid=eid, side=new_order_1.side, investment=investment, status=status, amount=new_order_1.amount, price=new_order_1.price).save()

            # NEW ORDER 2
            eid = exchange.create_order(pair.eid, new_order_2.amount, new_order_2.price, new_order_2.side)
            if eid == 0:
                Log.on('Order Creating Error - Exchange: '+exchange, 'error', investment.user)
            else:
                Order(eid=eid, side=new_order_2.side, investment=investment, status=status, amount=new_order_2.amount, price=new_order_2.price).save()

            investment.updated_at = datetime.datetime.now()
            investment.save()

            print('CHECK COMPLETED')

    def delete_user_exchange(self, user, exchange_name):
        if user in self._exchanges[exchange_name].keys():
            self._exchanges[exchange_name].pop(user)
        return True

    def main(self):
        self.investment_controller(self.get_orders_of_investments(self.get_unprocessed_investments()))

    def initial_orders(self, investment):
        pass


class Temp:
    pass

