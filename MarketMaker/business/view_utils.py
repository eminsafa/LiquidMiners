from .market_maker import MarketMaker
from .exchange_factory import ExchangeFactory
from ..models import *
import datetime

#MM = MarketMaker()
#EF = ExchangeFactory()


def get_total_liquidity(reward_id=False):
    total_liquidity = 0
    if reward_id is not False:
        investments = Investment.objects.filter(status__name='ACTIVE', reward_id=reward_id)
    else:
        investments = Investment.objects.filter(status__name='ACTIVE')
    for i in investments:
        total_liquidity += i.total_value
    return total_liquidity


def get_number_of_bots(reward_id=False):
    if reward_id is not False:
        investments = Investment.objects.filter(status__name='ACTIVE', reward_id=reward_id)
        return len(investments)
    else:
        return len(Investment.objects.filter(status__name='ACTIVE'))


def get_day_volume():
    investments = Investment.objects.all()
    return 1231023


def get_monthly_yield(reward_id=False):
    reward_of_month = get_reward_of_month(reward_id)
    total_investment = get_total_liquidity(reward_id)
    if reward_of_month > 0 and total_investment > 0:
        yield_amount = reward_of_month/total_investment*100
    else:
        yield_amount = 0
    return yield_amount


def get_hourly_yield(reward_id=False):
    return get_monthly_yield(reward_id) / 730


def get_all_pools():
    rewards = Reward.objects.filter(status__name='ACTIVE')
    for r in rewards:
        r.bots = get_number_of_bots(r.id)
        r.close_date = date_formatter(r.end_date)
        r.total_liquidity = get_total_liquidity(r.id)
        r.monthly_yield = r.get_monthly_yield(r.id)
        r.hourly_yield = r.get_hourly_yield(r.id)


def get_all_exchange_credentials(user):
    exchanges = EF.get_exchange_list()
    result = {}
    for exchange in exchanges:
        temp_credentials = UserMeta.get_api_credentials(user, exchange)
        result[exchange + '_public_key'] = temp_credentials['api_public_key']
        result[exchange + '_private_key'] = temp_credentials['api_private_key']
    return result

# HELPERS


def date_formatter(datetime):
    return str(datetime.day)+str(datetime.month)+(str(datetime.year)[-2:])


def time_formatter(datetime):
    return str(datetime.hour)+':'+str(datetime.minute)


def get_reward_of_month(reward_id=False):
    reward_total = 0
    start_week = Week.get(datetime.datetime.now())-3
    if reward_id is not False:
        reward_sharings = RewardSharing.objects.filter(week_id__gte=start_week, reward_id=reward_id)
    else:
        reward_sharings = RewardSharing.objects.filter(week_id__gte=start_week)
    for r in reward_sharings:
        reward_total += r.amount  # @todo convert to usd if not usd.
    return reward_total


def get_reward_of_week(reward_id=False):
    reward_total = 0
    start_week = Week.get(datetime.datetime.now())-1
    if reward_id is not False:
        reward_sharings = RewardSharing.objects.filter(week_id__gte=start_week, reward_id=reward_id)
    else:
        reward_sharings = RewardSharing.objects.filter(week_id__gte=start_week)
    for r in reward_sharings:
        reward_total += r.amount  # @todo convert to usd if not usd.
    return reward_total



