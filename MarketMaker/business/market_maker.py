import threading, time
from .trade_engine import TradeEngine
from .exchange_factory import ExchangeFactory
from ..models import *
import datetime
import json
from django.db.models import Sum
from django.contrib.auth import get_user_model
from django.shortcuts import get_object_or_404
from ..est_utils import *


@singleton
class MarketMaker:
    _debug_mode = False
    _trade_engine = ''
    _class_name = ''

    _months = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC']

    def __init__(self):
        self._class_name = 'MARKET_MAKER'  # @todo redundant
        self._exchange_factory = ExchangeFactory()
        self._trade_engine = TradeEngine()
        self.trade_engine_thread_initialize()
        self._run_thread = False
        self.exchange = self.get_user_exchange(User.objects.get(username='admin'), 'monetum')

    # =========================
    #
    # TRADE ENGINE THREAD
    #
    # =========================

    def trade_engine_thread(self):
        time.sleep(2)
        reward_sharing_counter = 0
        while True:
            if self._run_thread:
                print("\033[0;35m<<<<< ----+     New Iteration     ----- >>>>>\033[0m")
                try:
                    self._trade_engine.main()
                except Exception as e:
                    print("\033[0;31m"+f"{type(e).__name__} at line {e.__traceback__.tb_lineno} of {__file__}: {e}"+"\033[0m")
                if reward_sharing_counter % 500 == 0:
                    self.reward_sharing()
                    self.log_cleaner()
                reward_sharing_counter += 1
                time.sleep(5)
            else:
                if self._debug_mode:
                    time.sleep(30)
                else:
                    time.sleep(10)

    def trade_engine_thread_initialize(self):
        t1 = threading.Thread(target=self.trade_engine_thread)
        t1.start()


    # =========================
    #
    # CORE FUNCTIONS
    #
    # =========================

    def get_user_exchange(self, user, exchange_name):
        return self._exchange_factory.get(user, exchange_name)

    def delete_user_exchange(self, user, exchange_name):
        self._exchange_factory.delete(user, exchange_name)

    def get_trade_engine_status(self):
        print(self._run_thread)
        return self._run_thread

    def set_trade_engine_status(self, status_text):
        if status_text == 'on':
            print('ACTIVE')
            self._run_thread = True
        elif status_text == 'off':
            print('PASSIVE')
            self._run_thread = False

    def check_credentials(self, exchange_name, private_key, public_key):
        return self._trade_engine.check_credentials(exchange_name, private_key, public_key)


    def is_user_valid(self, user):
        return self._exchange_factory.is_user_valid(user)

    # @todo exchange
    def get_price(self, pair):
        time_diff = datetime.datetime.now() - datetime.timedelta(minutes=2)
        prices = Price.objects.filter(pair=pair, created_at__gte=time_diff)
        if len(prices) > 0:
            return prices[0].price
        else:
            buy_price, sell_price = self.exchange.get_dynamic_price(pair.eid, 0.0)
            mid_price = (buy_price + sell_price) / 2
            price = Price(pair=pair, price=mid_price)
            price.save()
            return mid_price

    def get_pairs(self, exchange_name, as_string=True):
        pairs = Pair.objects.filter(exchange=exchange_name)
        result = {}

        for pair in pairs:
            result[pair.eid] = pair.name

        if as_string:
            return json.dumps(result)
        else:
            return result

    def get_currencies(self, exchange_name, as_string=True):
        currencies = Currency.objects.filter(exchange=exchange_name)
        result = {}

        for currency in currencies:
            result[currency.eid] = currency.name

        if as_string:
            return json.dumps(result)
        else:
            return result


    # =========================
    #
    # FORM FUNCTIONS [CREATORS]
    #
    # =========================

    def create_investment(self, user, d):
        result = {}
        # @todo validation for balance is required
        previous_investments = Investment.objects.filter(user=user, status__name='ACTIVE')
        if len(previous_investments):
            result['error'] = 'ERROR: User have another active investment!'
            return result
        else:
            reward = Reward.objects.get(id=d['reward_id'])
            exchange = self.get_user_exchange(user, reward.exchange)
            balances = exchange.get_user_balance()
            crypto_amount = balances[reward.pair.buy_side.eid]
            fiat_amount = balances[reward.pair.sell_side.eid]
            crypto_value = exchange.get_price(reward.pair.eid).price * crypto_amount
            total_value = crypto_value + fiat_amount
            if total_value < 50.0:
                result['error'] = 'User balance is less than 50 EUR for this pair! Total Value of Wallet: ' + str(total_value)
                return result
            else:
                status = Status.objects.get(name='ACTIVE')
                Investment(
                    user=user,
                    reward=reward,
                    minimum_spread=d['minimum_spread'],
                    maximum_spread=d['maximum_spread'],
                    wallet_address=d['wallet_address'],
                    coin_amount=crypto_amount,
                    fiat_amount=fiat_amount,
                    total_value=total_value,
                    status=status
                ).save()
                result['success'] = 'Investment created successfully'
                try:
                    pass
                except:
                    result['error'] = 'Investment could not created!'

                return result

    def create_reward(self, data):
        dates = data['end-date'].split('/')
        campaign_duration = data['campaign_duration'] if 'campaign_duration' in data else None
        pair = Pair.objects.get(eid=data['pair'])
        reward = Reward(
            name=data['name'],
            amount=data['amount'],
            pair=pair,
            exchange=pair.exchange,
            currency=Currency.objects.get(eid=data['currency']),
            status=Status.objects.get(name='ACTIVE'),
            end_date=datetime.date(int(dates[2]), int(dates[1]), int(dates[0])),
            campaign_detail=data['campaign_detail'],
            rules=data['rules'],
            reward_detail=data['reward_detail'],
            campaign_duration=campaign_duration
        )
        reward.save()
        if 'week' and data['week']:
            for amount in data['week']:
                campaign_week_payment = CampaignWeekPayment(amount=amount)
                campaign_week_payment.save()
                reward.weekly_amount.add(campaign_week_payment)
        return {'success': 'Reward created successfully.'}

    def cancel_investment(self, user, reward_id):
        investments = Investment.objects.filter(user=user, reward_id=reward_id, status__name='ACTIVE')
        if len(investments) == 0:
            return {'error': 'Investment already cancelled!'}
        elif len(investments) != 1:
            return {'error': 'Theree are multiple investment with same conditions!'}
        else:
            investment = investments[0]
            investment.status = Status.objects.get(name='CANCELLED')
            investment.save()
            return {'success': 'Investment cancelled!'}

    # =========================
    #
    # DISPLAY FUNCTIONS
    #
    # =========================

    # Rewards Page for Investors
    def display_rewards_investors(self, user):
        result = {
            'rewards': [],
            'balance': [],
            'order': []
        }
        rewards = Reward.objects.filter()

        for reward in rewards:
            r = Temp()
            r.id = reward.id
            r.pair = reward.pair.name
            r.end_date = reward.end_date
            r.end_date_year = reward.end_date.year
            r.end_date_month = reward.end_date.month
            r.end_date_day = reward.end_date.day
            r.buy_side = reward.pair.buy_side.symbol
            r.buy_side_icon = r.buy_side.lower()
            r.sell_side = reward.pair.sell_side.symbol
            r.sell_side_icon = r.sell_side.lower()
            r.amount = str(reward.amount) + ' ' + reward.currency.symbol
            r.trades = reward.get_order_count()
            r.currency = reward.currency.name
            r.status = reward.status.name
            r.status_style = reward.status.style
            r.investments = reward.get_investments_total()
            if r.investments > 0:
                r.percentage_yield = round(float(reward.amount) / float(r.investments) * 100, 2)
            else:
                r.percentage_yield = 'N/A'
            r.investors = len(Investment.objects.filter(reward=reward))

            if reward.is_user_invested(user):
                user_investment = Investment.objects.get(reward=reward, user=user, status__name='ACTIVE')
                r.user_investment = str(user_investment.total_value) + ' ' + user_investment.reward.pair.sell_side.symbol
                r.action_text = 'Investment Details'
                r.action_page = 'reward-details'
                r.row_class = 'invested-reward'
            else:
                r.action_text = 'Invest'
                r.action_page = 'new-investment'
            result['rewards'].append(r)

        balances = self.get_user_exchange(user, reward.exchange).get_user_balance()
        for i in balances:
            b = Temp()
            print('B', b)
            b.name = Currency.objects.get(eid=i).symbol
            # try:
            #     b.name = get_object_or_404(Currency, id=i).symbol
            # except Currency.DoesNotExist:
            #     return False
            b.icon = b.name.lower()
            b.balance = balances[i]
            b.total_balance = balances[i]
            result['balance'].append(b)
        orders = self.get_user_exchange(user, reward.exchange).api('order/list')['response']['entities']  # @todo had to update
        for i in orders:
            b = Temp()
            pair_id = i['pair_id']
            pair = get_object_or_404(Pair, eid=pair_id)
            b.id = i['order_id']
            b.coin = pair.buy_side.symbol
            b.coin_icon = b.coin.lower()
            b.fiat = pair.sell_side.symbol
            b.fiat_icon = b.fiat.lower()
            b.type = i['type'].capitalize()
            b.price = round(float(i['price']), 2)
            b.amount = i['volume']
            result['order'].append(b)
        return result

    def display_rewards_public(self):
        result = {
            'rewards': [],
        }
        rewards = Reward.objects.all()

        for reward in rewards:
            r = Temp()
            r.id = reward.id
            r.pair = reward.pair.name
            r.end_date = reward.end_date
            r.end_date_year = reward.end_date.year
            r.end_date_month = reward.end_date.month
            r.end_date_day = reward.end_date.day
            r.buy_side = reward.pair.buy_side.symbol
            r.buy_side_icon = r.buy_side.lower()
            r.sell_side = reward.pair.sell_side.symbol
            r.sell_side_icon = r.sell_side.lower()
            r.amount = str(reward.amount) + ' ' + reward.currency.symbol
            r.trades = reward.get_order_count()
            r.currency = reward.currency.name
            r.status = reward.status.name
            r.status_style = reward.status.style
            r.investments = reward.get_investments_total()
            if r.investments > 0:
                r.percentage_yield = round(float(reward.amount) / float(r.investments) * 100, 2)
            else:
                r.percentage_yield = 'N/A'
            r.investors = len(Investment.objects.filter(reward=reward))

            result['rewards'].append(r)

        return result

    def display_new_investment(self, user, reward_id):
        result = {}
        try:
            reward = Reward.objects.get(id=reward_id)
            exchange = self.get_user_exchange(user, reward.exchange)
            r = Temp()

            r.reward_id = reward_id
            r.pair = reward.pair.name
            r.reward_amount = reward.amount
            r.reward_currency = reward.currency.symbol
            r.end_date = reward.end_date
            balances = exchange.get_user_balance()
            if 'error' not in balances.keys():
                if reward.pair.buy_side.eid in balances.keys():
                    r.crypto_amount = balances[reward.pair.buy_side.eid]
                else:
                    r.crypto_amount = 0.0
                r.crypto_name = reward.pair.buy_side.symbol
                if reward.pair.sell_side.eid in balances.keys():
                    r.fiat_amount = balances[reward.pair.sell_side.eid]
                else:
                    r.fiat_amount = 0.0
                r.fiat_name = reward.pair.sell_side.symbol
                r.crypto_value = exchange.get_price(reward.pair.id).price * r.crypto_amount
                r.total_value = round(r.crypto_value + r.fiat_amount, 6)
            result['r'] = r
        except:
            result['error'] = 'Reward not found!'

        return result

    def display_reward_details_public(self, reward_id):
        result = {
            'orders': []
        }

        reward = Reward.objects.get(id=reward_id)
        orders = Order.objects.filter(investment__reward=reward).order_by('-created_at')[:50]

        result['total_investments'] = round(reward.get_investments_total(), 4)
        result['reward_amount'] = str(reward.amount) + ' ' + reward.currency.symbol
        result['trade_number'] = len(Order.objects.filter(investment__reward=reward))
        result['earned_reward'] = 0
        result['pair'] = reward.pair.name
        result['pair_end'] = reward.end_date
        result['currency'] = reward.pair.sell_side.symbol

        for order in orders:
            o = Temp()
            o.datetime = order.created_at
            o.pair = reward.pair.name
            o.side = order.side.capitalize()
            o.price = order.price
            o.status = order.status.name
            o.status_style = order.status.style
            o.amount = order.amount
            if o.side == 'Buy':
                o.side_style = 'success'
            else:
                o.side_style = 'danger'
            result['orders'].append(o)

        return result

    def display_admin_reward_details(self, reward_id):
        result = {
            'orders': [],
            'weeks': []
        }

        reward = Reward.objects.get(id=reward_id)
        orders = Order.objects.filter(investment__reward=reward).order_by('-created_at')[:50]

        result['total_investments'] = round(reward.get_investments_total(), 4)
        result['reward_amount'] = str(reward.amount) + ' ' + reward.currency.symbol
        result['trade_number'] = len(Order.objects.filter(investment__reward=reward))
        result['earned_reward'] = 0
        result['pair'] = reward.pair.name
        result['pair_end'] = reward.end_date
        result['currency'] = reward.pair.sell_side.symbol

        for order in orders:
            o = Temp()
            o.datetime = order.created_at
            o.pair = reward.pair.name
            o.side = order.side.capitalize()
            o.price = order.price
            o.amount = order.amount
            o.status = order.status.name
            o.status_style = order.status.style
            if o.side == 'Buy':
                o.side_style = 'success'
            else:
                o.side_style = 'danger'
            result['orders'].append(o)

        result['investments'] = Investment.objects.filter(reward=reward_id)
        dt = datetime.datetime.now()
        current_week = Week.get(dt).id - 1
        start_week = Week.get(reward.start_date).id
        end_week = Week.get(reward.end_date).id
        if end_week < current_week:
            current_week = end_week
        for week in range(start_week, current_week + 1):
            w = Temp()
            week = Week.objects.get(id=week)
            w.start = week.start
            w.end = week.end
            w.id = week.id
            w.reward = reward
            w.status = Transaction.objects.get(week=week, reward=reward)
            w.transactions = RewardSharing.objects.filter(week=week, reward=reward)

            result['weeks'].append(w)
        return result

    def display_investment_details(self, user, reward_id):
        result = {
            'orders': [],
            'earnings': [],
            'balance': []
        }

        reward = Reward.objects.get(id=reward_id)
        investment = Investment.objects.get(reward=reward, user=user, status__name='ACTIVE')
        orders = Order.objects.filter(investment=investment).order_by('-created_at')[:50]
        reward_sharings = RewardSharing.objects.filter(investment=investment)

        result['total_investments'] = round(reward.get_investments_total(), 4)
        result['reward_id'] = reward_id
        result['reward_amount'] = str(reward.amount) + ' ' + reward.currency.symbol
        result['trade_number'] = len(Order.objects.filter(investment=investment, status__name='FILLED'))
        result['earned_reward'] = investment.get_total_earnings()
        result['pair'] = reward.pair.name
        result['pair_end'] = reward.end_date
        result['currency'] = reward.pair.sell_side.symbol

        for order in orders:
            o = Temp()
            o.datetime = order.created_at
            o.pair = reward.pair.name
            o.side = order.side.capitalize()
            o.price = order.price
            o.status = order.status.name
            o.status_style = order.status.style
            o.amount = order.amount
            if o.side == 'Buy':
                o.side_style = 'success'
            else:
                o.side_style = 'danger'
            result['orders'].append(o)

        balances = self.get_user_exchange(user, reward.exchange).get_user_balance()
        for i in balances:
            b = Temp()
            b.name = Currency.objects.get(id=i).symbol
            b.icon = b.name.lower()
            b.balance = balances[i]
            b.total_balance = balances[i]
            result['balance'].append(b)

        for sharing in reward_sharings:
            r = Temp()
            r.start = sharing.week.start
            r.end = sharing.week.end
            r.reward_achieved = sharing.amount
            r.currency = reward.currency.symbol
            transaction_obj = Transaction.objects.get(week=sharing.week, reward=sharing.reward)
            r.transaction_status = transaction_obj.status.name
            r.transaction_style = transaction_obj.status.style
            r.wallet_address = investment.wallet_address

            result['earnings'].append(r)
        return result

    def display_admin_dashboard(self, user):
        exchange = self.get_user_exchange(user, 'monetum')
        response = exchange.api('orderbook/info?pair_id=7', method='GET')
        result = {'datax': []}
        for e in response['response']['entities']['asks']:
            a = Temp()
            a.amount = e['amount']
            a.price = e['price']
            a.type = 'ask'
            result['datax'].append(a)

        for e in response['response']['entities']['bids']:
            a = Temp()
            a.amount = e['amount']
            a.price = e['price']
            a.type = 'bid'
            result['datax'].append(a)
        result['data'] = []
        counter = 0
        for i in response['response']['entities']['asks']:
            r = Temp()
            r.ask = i['price']
            if counter <= len(response['response']['entities']['bids']) - 1:
                r.bid = response['response']['entities']['bids'][counter]['price']
            else:
                r.bid = response['response']['entities']['bids'][0]['price']
            counter += 1
            result['data'].append(r)
        result['total_investment'] = round(Investment.get_total_all(), 2)
        result['investments'] = []
        now = datetime.datetime.now()
        c_year = now.year
        c_month = now.month
        for i in range(6):
            a = Temp()
            investments = Investment.objects.filter(created_at__year=c_year, created_at__month=c_month)
            total = 0
            for n in investments:
                total += n.total_value
            a.total = total
            a.month = self._months[c_month - 1]
            if c_month == 1:
                c_month = 13
                c_year -= 1
            c_month -= 1
            result['investments'].append(a)
        result['investments'].reverse()
        User = get_user_model()
        users = User.objects.all()
        result['customers'] = len(users)

        rewards = RewardSharing.objects.all()
        rew_total = 0
        for i in rewards:
            rew_total += i.amount
        result['reward_total'] = rew_total

        result['order_count'] = Order.objects.filter(status__name='FILLED').count()

        return result

    def display_transacitons(self):
        rewards = Reward.objects.all()
        result = {'rewards': rewards}
        return result

    # =========================
    #
    # INITIALIZERS
    #
    # =========================

    def initialize(self, force=True):
        from django.db import connections

        cursor = connections['default'].cursor()
        cursor.execute("UPDATE SQLITE_SEQUENCE SET SEQ=0 WHERE NAME='MarketMaker_week';")
        cursor.execute("UPDATE SQLITE_SEQUENCE SET SEQ=0 WHERE NAME='MarketMaker_status';")
        cursor.execute("UPDATE SQLITE_SEQUENCE SET SEQ=0 WHERE NAME='MarketMaker_currency';")
        cursor.execute("UPDATE SQLITE_SEQUENCE SET SEQ=0 WHERE NAME='MarketMaker_pair';")

        Currency.objects.all().delete()
        Pair.objects.all().delete()

        admin_user = User.objects.get(username='admin')

        self.initialize_usermeta(admin_user)
        self.initialize_database()

        # Exchange Synchronize

        self.sync_monetum_exchange(admin_user)
        self.sync_gateio_exchange(admin_user)

    def initialize_usermeta(self, user):
        usermetas = UserMeta.objects.filter(user=user, type='monetum_private_key')
        if len(usermetas) == 0:
             UserMeta(user=user, type='monetum_private_key', meta='64s9kyblva6yxddz7x31vwlslhxmurm8ngj2okewvepezppza2hpvwtjgwxiaf9h').save()
             UserMeta(user=user, type='monetum_public_key', meta='93XIXE8SYGUOFSP7P0F7I2MBAS42F3GYCBQ9TEMJOPE1LLNDYCTJNNHHWR7JQAW7').save()

        usermetas = UserMeta.objects.filter(user=user, type='gateio_private_key')
        if len(usermetas) == 0:
             UserMeta(user=user, type='gateio_private_key', meta='e8d455739c22c9115e2b62c51c81bb354b4b850b2233b13f0ff6c99a6f56ed1e').save()
             UserMeta(user=user, type='gateio_public_key', meta='011795083abb4e712d54e9f783823ff0').save()

    def initialize_database(self):
        # Status
        statuses = {'ACTIVE': 'success', 'PASSIVE': 'danger', 'PENDING': 'warning', 'CANCELLED': 'danger', 'FILLED': 'light',
                    'PROCESSED': 'success', 'PROCESSING': 'warning', 'NOT PROCESSED': 'danger'}

        Status.objects.all().delete()
        for s in statuses:
            Status(name=s, style=statuses[s]).save()

        from datetime import timedelta, datetime
        years = [2021, 2022, 2023]

        Week.objects.all().delete()
        for year in years:
            d = datetime(year, 1, 1)  # January 1st
            d = d + timedelta(days=7 - d.weekday())  # First Monday
            while d.year == year:
                s = d + timedelta(days=6)
                s = s.replace(hour=23, minute=59, second=59, microsecond=999999)
                Week(start=d, end=s).save()
                d += timedelta(days=7)

    def sync_monetum_exchange(self, user):
        exchange = self.get_user_exchange(user, 'monetum')

        # Currency Sync
        result = exchange.get_currencies()
        if 'error' not in result.keys():
            for c in result['currencies']:
                currency_check = Currency.objects.filter(eid=c['currency_id'], exchange='monetum')
                if len(currency_check) == 0:
                    Currency(eid=c['currency_id'], name=c['name'], symbol=c['iso'], icon=c['iso'].lower(), exchange='monetum').save()
        # Pair Sync
        result = exchange.get_pairs()
        if 'error' not in result.keys():
            for p in result['pairs']:
                pair_check = Pair.objects.filter(eid=p['pair_id'], exchange='monetum')
                if len(pair_check) == 0:
                    from_currency = Currency.objects.get(eid=p['currency_id_from'], exchange='monetum')
                    to_currency = Currency.objects.get(eid=p['currency_id_to'], exchange='monetum')
                    pair_name = from_currency.symbol + '/' + to_currency.symbol
                    Pair(eid=p['pair_id'], name=pair_name, buy_side=from_currency, sell_side=to_currency, exchange='monetum').save()

    def sync_gateio_exchange(self, user):
        exchange = self.get_user_exchange(user, 'gateio')

        # Currency Sync
        desired_currencies = ['MIMIR', 'USDT', 'USD', 'BNB', 'XRP', 'ETH', 'LTC', 'GT', 'BTC', 'EUR']
        currencies = exchange.get_currencies()
        print(currencies)
        for c in currencies:
            if c in desired_currencies:
                currency_check = Currency.objects.filter(eid=c, exchange='gateio')
                if len(currency_check) == 0:
                    Currency(eid=c, name=c, symbol=c, icon=c.lower(), exchange='gateio').save()


        # Pair Sync
        desired_pairs = ['MIMIR_USDT', 'BNB_USDT', 'XRP_USDT', 'MIMIR_ETH', 'LTC_USDT', 'GT_USDT', 'BTC_USDT', 'ETH_USDT']  # We will use only these pairs.
        pairs = exchange.get_pairs()
        for p in pairs:
            if p['id'] in desired_pairs:
                pair_check = Pair.objects.filter(eid=p['id'], exchange='gateio')
                if len(pair_check) == 0:
                    a = p['base']
                    try:
                        from_currency = Currency.objects.get(eid=p['base'], exchange='gateio')
                        to_currency = Currency.objects.get(eid=p['quote'], exchange='gateio')
                    except:
                        print(p['base'] + " or " + p['quote'] + " does not exist!")
                    else:
                        pair_name = from_currency.symbol + '/' + to_currency.symbol
                        Pair(eid=p['id'], name=pair_name, buy_side=from_currency, sell_side=to_currency, exchange='gateio').save()


    # =========================
    #
    # TEST FUNCTIONS
    #
    # =========================

    def test_exchange(self, user, endpoint='trade/list'):
        exchange = self.get_user_exchange(user)
        # result = exchange.get_currencies()
        # self.sync_exchange_data(user)
        # response = exchange.api('order/new', {'pair_id': 7, 'amount': 0.00137664, 'price': 33500, 'type': 'sell'})
        # response = {'BOK': 'YEA'}
        # order = Order.objects.get(eid=15646400)
        # response = exchange.cancel_order(order)
        response = exchange.api('trade/list')
        ### {'errors': False, 'response': {'entity': {'order_id': 15615704, 'pair_id': 7, 'type': 'sell', 'type_trade': 'limit', 'volume': '0.00137660', 'price': '33500.00', 'fee_percent': '0.5000', 'total': '46.11610000'}}}
        return response

    # =========================
    #
    # ADVANCED FUNCTIONS
    #
    # =========================

    def calculate_reward_sharing(self, reward, week_id=False):
        if not week_id:
            week = Week.get(datetime.datetime.now())
        else:
            week = Week.objects.get(id=week_id)

        weeks_number = len(Week.gets_of_reward(reward))
        weekly_reward = float(reward.amount) / float(weeks_number)

        # Check if it calculated before
        sharings = RewardSharing.objects.filter(reward=reward, week=week)
        if len(sharings) > 0:
            # It is calculated before!
            Log(type='error', message='Sharing already exist. REWARD_ID:' + str(reward.id) + ' WEEK:' + str(week.id)).save()
        else:
            try:
                total_investment = reward.get_investments_total()
                total_spent_time = reward.get_spent_time(week)
                if total_spent_time == 0:
                    total_spent_time = 1
                if total_investment > 0:
                    investments = Investment.objects.filter(reward=reward)
                    for i in investments:
                        st = reward.get_user_spent_time(week, i.user)
                        ratio = (i.total_value * st) / (total_investment * total_spent_time)
                        print(ratio)
                        reward_amount = ratio * weekly_reward
                        if reward_amount <= weekly_reward:
                            RewardSharing(investment=i, reward=reward, ratio=ratio, amount=reward_amount, week=week).save()
                        else:
                            Log(type='error', message='CRITICAL: Logic Error. MM.calculate_reward_sharing').save()
                Log(type='info', message='Reward calculated! for reward: ' + str(reward.id) + ' week: ' + str(week)).save()
            except:
                Log(type='error', message='CRITICAL: Error occurred during RewardSharing calculation! Check Data').save()

    def reward_sharing(self):
        print("\033[0;32m<<<<< ----+  SharingsCalculation  ----- >>>>>\033[0m")
        rewards = Reward.objects.all()
        dt = datetime.datetime.now()
        current_week = Week.get(dt).id - 1
        for r in rewards:
            start_week = Week.get(r.start_date).id
            end_week = Week.get(r.end_date).id
            if end_week < current_week:
                current_week = end_week
            for week in range(start_week, current_week + 1):
                sharings = RewardSharing.objects.filter(week=week, reward=r)
                if len(sharings) == 0:
                    self.calculate_reward_sharing(r, week)

        # INVESTMENT STATUS CHECK

        investments = Investment.objects.filter(reward__end_date__lt=datetime.datetime.now(), status__name='ACTIVE')
        reward_objects = Reward.objects.filter(end_date__lt=datetime.datetime.now(), status__name='ACTIVE')
        passive = Status.objects.get(name='PASSIVE')
        for i in investments:
            i.status = passive
            i.save()
        for r in reward_objects:
            r.status = passive
            r.save()
        print("\033[0;32m          | Done.\033[0m")

    def log_cleaner(self):
        APILog.objects.filter(timestamp__lte=datetime.datetime.now() - datetime.timedelta(days=7)).delete()
        Log.objects.filter(timestamp__lte=datetime.datetime.now() - datetime.timedelta(days=21)).delete()

    # =========================
    #
    # NEW CLEAN FUNCTIONS
    #
    # =========================

    def get_total_liquidity(self, reward_id=False):
        total_liquidity = 0
        if reward_id is not False:
            investments = Investment.objects.filter(status__name='ACTIVE', reward_id=reward_id)
        else:
            investments = Investment.objects.filter(status__name='ACTIVE')
        for i in investments:
            total_liquidity += i.total_value
        return round(total_liquidity, 2)

    def get_number_of_bots(self, reward_id=False):
        if reward_id is not False:
            investments = Investment.objects.filter(status__name='ACTIVE', reward_id=reward_id)
            return len(investments)
        else:
            return len(Investment.objects.filter(status__name='ACTIVE'))

    def get_day_volume(self):
        total_day_volume = 0
        date_from = datetime.datetime.now() - datetime.timedelta(days=1)
        orders = Order.objects.filter(created_at__gte=date_from)
        for o in orders:
            total_day_volume += o.amount * o.price
        return round(total_day_volume, 2)

    def get_monthly_yield(self, reward_id=False):
        reward_of_month = self.get_reward_of_month(reward_id)
        total_investment = self.get_total_liquidity(reward_id)
        if reward_of_month > 0 and total_investment > 0:
            yield_amount = reward_of_month / total_investment * 100
        else:
            yield_amount = 0
        return round(yield_amount, 2)

    def get_weekly_yield(self, reward_id=False):
        reward_of_week = self.get_reward_of_week(reward_id)
        total_investment = self.get_total_liquidity(reward_id)
        if reward_of_week > 0 and total_investment > 0:
            yield_amount = reward_of_week / total_investment * 100
        else:
            yield_amount = 0
        return round(yield_amount, 2)

    def get_hourly_yield(self, reward_id=False):
        yield_amount = self.get_monthly_yield(reward_id) / 730
        if yield_amount == 0:
            yield_amount = 0.0
        return round(yield_amount, 2)

    def get_all_pools(self):
        rewards = Reward.objects.filter(status__name='ACTIVE')
        for r in rewards:
            r.bots = self.get_number_of_bots(r.id)
            r.close_date = self.date_formatter(r.end_date)
            r.close_hour = self.time_formatter(r.end_date)
            r.liquidity = self.get_total_liquidity(r.id)
            r.monthly_yield = self.get_monthly_yield(r.id)
            r.hourly_yield = self.get_hourly_yield(r.id)
        return {'pools': rewards}

    def get_rewardsharings(self, reward_id, user):
        reward = Reward.objects.get(id=reward_id)
        investment = Investment.objects.get(reward=reward_id, user=user, status__name='ACTIVE')
        start_week = Week.get(reward.start_date).id
        sharings = RewardSharing.objects.filter(reward_id=reward_id, investment__user=user)

        wallet_address = investment.wallet_address
        duration = datetime.date.today() - reward.start_date
        for s in sharings:
            s.week_number = s.week.id - start_week + 1
            s.transaction = Transaction.get_status(reward, s.week)
            s.wallet_address = wallet_address[0:4]
            s.duration = duration.days
        return {'sharings': sharings}

    def get_orders(self, reward_id, user):
        investment = Investment.objects.get(reward=reward_id, user=user, status__name='ACTIVE')
        reward = investment.reward
        orders = Order.objects.filter(investment=investment).order_by('-id')[:20]
        for o in orders:
            o.reward = reward
            if o.side == 'buy':
                o.side = 'Buy'
                o.side_style = 'success'
            else:
                o.side = 'Sell'
                o.side_style = 'danger'
        return {'orders': orders}

    def date_formatter(self, datetime):
        return str(datetime.day) + '/' + str(datetime.month) + '/' + (str(datetime.year)[-2:])

    def time_formatter(self, datetime):
        return '23:59'

    def get_reward_of_month(self, reward_id=False, currency_id=False):
        if currency_id is False:
            currency_id = Currency.objects.get(symbol='EUR').id
        reward_total = 0
        start_week = Week.get(datetime.datetime.now()).id - 3
        if reward_id is not False:
            reward_sharings = RewardSharing.objects.filter(week_id__gte=start_week, reward_id=reward_id)
        else:
            reward_sharings = RewardSharing.objects.filter(week_id__gte=start_week)

        price_info = {}  # <Reward>: price (int)

        for rs in reward_sharings:
            reward = rs.reward

            if reward.currency_id != currency_id:
                if reward in price_info.keys():
                    price = price_info[reward]
                else:
                    try:
                        pairs = Pair.objects.filter(buy_side=reward.pair.buy_side, sell_side=currency_id)
                        if len(pairs) != 1:
                            pairs = Pair.objects.filter(buy_side=currency_id, sell_side=reward.pair.buy_side)
                            if len(pairs) != 1:
                                Log(type='error', message='get_reward_of_month error. Pairs not found! (1)').save()
                            else:
                                pair = pairs[0]
                        else:
                            pair = pairs[0]
                    except:
                        Log(type='error', message='get_reward_of_month error. Pair not found! (2)').save()
                        return 0
                    price = self.get_price(pair)
                    price_info[reward] = price
                reward_total += rs.amount * price
            else:
                reward_total += rs.amount

        return reward_total

    def get_reward_of_week(self, reward_id=False):
        reward_total = 0
        start_week = Week.get(datetime.datetime.now()).id - 1
        if reward_id is not False:
            reward_sharings = RewardSharing.objects.filter(week_id__gte=start_week, reward_id=reward_id)
        else:
            reward_sharings = RewardSharing.objects.filter(week_id__gte=start_week)
        for r in reward_sharings:
            reward_total += r.amount  # @todo convert to usd if not usd.
        return reward_total

    def get_week_of_reward(self, reward_id, safe=True):
        reward = Reward.objects.get(id=reward_id)
        current_week = Week.get(datetime.datetime.now())
        if safe:
            if datetime.date.today().__gt__(reward.end_date):
                week = Week.get(reward.end_date)
        week = current_week.id - Week.get(reward.start_date).id + 1
        if week < 1:
            Log(type='error', message='Week calculation error. MM.get_week_of_reward').save()
            week = 1
        return week

    def get_user_balance_for_reward(self, user, reward):
        exchange = self.get_user_exchange(user, reward.exchange)
        balances = exchange.get_user_balance()
        balance = Temp()
        if reward.pair.buy_side.eid in balances.keys():
            balance.base_amount = balances[reward.pair.buy_side.eid]
        else:
            balance.base_amount = 0.0
        balance.base = reward.pair.buy_side
        if reward.pair.sell_side.eid in balances.keys():
            balance.quote_amount = balances[reward.pair.sell_side.eid]
        else:
            balance.quote_amount = 0.0
        balance.quote = reward.pair.sell_side
        balance.base_value = exchange.get_price(reward.pair.eid).price * balance.base_amount
        balance.total_value = round(balance.base_value + balance.quote_amount, 6)

        balance.base_amount_rounded = round(balance.base_amount, 3)
        balance.quote_amount_rounded = round(balance.quote_amount, 3)
        balance.base_value_rounded = round(balance.base_value, 3)
        balance.total_value_rounded = round(balance.total_value, 3)

        return {'balance': balance}

    def get_current_share_of_pool(self, reward, user_total_value):
        total_liquidity = self.get_total_liquidity(reward.id)
        if total_liquidity < 1:
            if user_total_value < 1:
                return 100
            else:
                total_liquidity = user_total_value
        else:
            total_liquidity += user_total_value
        return round(user_total_value / total_liquidity * 100, 3)

    def api_cancel_order(self, user_id, order_eid, order_id):
        users = User.objects.filter(id=user_id)
        if len(users) == 1:
            user = users[0]
            orders = Order.objects.filter(id=order_id, eid=order_eid)
            if len(orders) == 1:
                order = orders[0]
                exchange = self.get_user_exchange(user, order.investment.reward.pair.exchange)
                cancel_result = exchange.cancel_order(order)
                if cancel_result:
                    order.status = Status.objects.get(name='CANCELLED')
                    order.save()
                    return '{"error":false, "message":"Order (' + str(order.eid) + ') cancelled successfully!"}'
                else:
                    return '{"error":true, "message":"Could not cancelled!"}'
            else:
                return '{"error":true, "message":"Order not found!"}'
        else:
            return '{"error":true, "message":"Unauthorized!"}'

    def get_unique_username(self, email):
        import re
        email = re.sub('[!@#$*=()?^+%&/.]', '', email)
        users = User.objects.filter(username=email)
        if len(users) == 0:
            return email
        else:
            return self.get_unique_username(email + '1')


class Temp:
    pass
