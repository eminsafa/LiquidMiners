import requests
import time
import hashlib
import hmac

def gen_sign(method, url, query_string=None, payload_string=None):
    # EST {"apiKey":"57dd99c9bb68788ad9b51ec8afd179a5","secretKey":"a11c2a8755431ea590eae4f48c8a4cb6fa5c62a04f66f32b78ddc09845072b19"}
    key = '57dd99c9bb68788ad9b51ec8afd179a5'        # api_key
    secret = 'a11c2a8755431ea590eae4f48c8a4cb6fa5c62a04f66f32b78ddc09845072b19'     # api_secret

    t = time.time()
    m = hashlib.sha512()
    m.update((payload_string or "").encode('utf-8'))
    hashed_payload = m.hexdigest()
    s = '%s\n%s\n%s\n%s\n%s' % (method, url, query_string or "", hashed_payload, t)
    sign = hmac.new(secret.encode('utf-8'), s.encode('utf-8'), hashlib.sha512).hexdigest()
    return {'KEY': key, 'Timestamp': str(t), 'SIGN': sign}



host = "https://api.gateio.ws"
prefix = "/api/v4"
headers = {'Accept': 'application/json', 'Content-Type': 'application/json'}

url = '/wallet/total_balance'
query_param = ''
# for `gen_sign` implementation, refer to section `Authentication` above
sign_headers = gen_sign('GET', prefix + url, query_param)
headers.update(sign_headers)
r = requests.request('GET', host + prefix + url, headers=headers)
print(r.json())

url = '/spot/currencies'
query_param = ''
r = requests.request('GET', host + prefix + url, headers=headers)
print(r.json())
