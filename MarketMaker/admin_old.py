from django.contrib import admin
from .models import *


class StatusAdmin(admin.ModelAdmin):
    list_display = ('name', 'style')


class WeekAdmin(admin.ModelAdmin):
    list_display = ('start', 'end')


class CurrencyAdmin(admin.ModelAdmin):
    list_display = ('eid', 'symbol', 'name')


class PairAdmin(admin.ModelAdmin):
    list_display = ('eid', 'name', 'buy_side', 'sell_side', 'buy_side_step', 'sell_side_step')


class RewardAdmin(admin.ModelAdmin):
    list_display = ('pair', 'name', 'amount', 'currency', 'start_date', 'end_date', 'status')


class InvestmentAdmin(admin.ModelAdmin):
    list_display = ('user', 'reward', 'minimum_spread', 'maximum_spread', 'wallet_address', 'coin_amount', 'fiat_amount', 'total_value', 'status', 'created_at', 'updated_at', 'buy_weight', 'sell_weight')


class OrderAdmin(admin.ModelAdmin):
    list_display = ('investment', 'eid', 'amount', 'price', 'side', 'status', 'created_at', 'updated_at')


class RewardSharingAdmin(admin.ModelAdmin):
    list_display = ('reward', 'investment', 'ratio', 'amount', 'week')


class LogAdmin(admin.ModelAdmin):
    list_display = ('user', 'timestamp', 'type', 'message')


class APILogAdmin(admin.ModelAdmin):
    list_display = ('user', 'timestamp', 'endpoint', 'params', 'response')


class UserMetaAdmin(admin.ModelAdmin):
    list_display = ('user', 'type', 'meta')


class TransactionAdmin(admin.ModelAdmin):
    list_display = ('reward', 'week', 'status')


class PriceAdmin(admin.ModelAdmin):
    list_display = ('pair', 'created_at', 'price')

admin.site.register(Status, StatusAdmin)
admin.site.register(Week, WeekAdmin)
admin.site.register(Currency, CurrencyAdmin)
admin.site.register(Pair, PairAdmin)
admin.site.register(Reward, RewardAdmin)
admin.site.register(Investment, InvestmentAdmin)
admin.site.register(Order, OrderAdmin)
admin.site.register(RewardSharing, RewardSharingAdmin)
admin.site.register(Log, LogAdmin)
admin.site.register(APILog, APILogAdmin)
admin.site.register(UserMeta, UserMetaAdmin)
admin.site.register(Transaction, TransactionAdmin)
admin.site.register(Price, PriceAdmin)
