
from django.shortcuts import redirect

def singleton(class_):
    instances = {}

    def get_instance(*args, **kwargs):
        if class_ not in instances:
            instances[class_] = class_(*args, **kwargs)
        return instances[class_]

    return get_instance


#from .business.market_maker import MarketMaker
#MM = MarketMaker()

def authorized(function):
    def wrapper(request):
        if login_check(request):
            return login_check(request)
        return function(request)
    return wrapper


# @ admin
def admin(function):
    def wrapper(request):
        result = admin_check(request)
        if result:
            return result
        return function(request)

    return wrapper


def login_check(request):
    if not request.user.is_authenticated:
        response = redirect('/login/')
        return response
    return False


def admin_check(request):
    if not request.user.is_superuser:
        response = redirect('/home/')
        return response
    return False


def is_user_valid(request):
    if not MM.is_user_valid(request.user):
        response = redirect('/settings/')
        return response


def cache(function):
    def wrapper():
        pass


