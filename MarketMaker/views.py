from django.shortcuts import render
from django.shortcuts import HttpResponse
from django.shortcuts import redirect
from django.contrib.auth import logout
from .forms import CustomUserCreationForm, CustomPasswordChangeForm
from .models import *
from django.contrib.auth.hashers import make_password
from django.shortcuts import get_object_or_404
from django.contrib.auth import views as auth_views
from .forms import LoginForm
from .est_utils import *
from .business import view_utils

import json




# =========================
#
# CORE FUNCTIONS
#
# =========================




class LoginView(auth_views.LoginView):
    form_class = LoginForm
    template_name = 'pages/login.html'


@admin
def initialize(request):
    MM.initialize()
    return HttpResponse('OK')


def user_logout(request):
    logout(request)
    return redirect('login')


# =========================
#
# PUBLIC PAGES
#
# =========================


def register(request):
    result = {}
    if request.method == 'POST':
        data = request.POST
        if len(User.objects.filter(email=data['email'])) != 0:
            return {'error': 'This e-mail already registered!'}
        if data['password1'] != data['password2']:
            return {'error': 'Passwords does not match!'}
        try:
            user = User.objects.create_user(data['email'], data['email'], data['password1'])
        except:
            Log(type='ERROR', message='Could not registered due to user creation!').save()
            return {'error': 'User could not registered!'}

        result['success'] = 'User created'
        redirect('login')
    else:
        form = CustomUserCreationForm()
        result['form'] = form
    return render(request, 'pages/register.html', result)


def home(request):
    result = {
        'active_page': 'home',
        'title': 'Home'
    }

    auth_check = admin_check(request)
    if not auth_check:
        result['is_admin'] = True

    result['total_liquidity'] = MM.get_total_liquidity()
    result['number_of_bots'] = MM.get_number_of_bots()
    result['day_volume'] = MM.get_day_volume()
    result['weekly_yield'] = MM.get_weekly_yield()
    result['hourly_yield'] = MM.get_hourly_yield()
    result = {**result, **MM.get_all_pools()}

    return render(request, 'pages/home.html', result)
    # response = login_check(request)
    # if not response:
    #    auth_check = admin_check(request)
    #    if auth_check:
    #        return redirect('/liquidity_pools/')
    #    else:
    #        return redirect('/admin/dashboard/')
    # else:
    #    return redirect('/liquidity_pools/')


# =========================
#
# DYNAMIC PAGES
#
# =========================

@authorized
@admin
def admin_dashboard(request):
    result = {'active_page': 'admin_dashboard', 'is_admin': True}
    result = {**result, **MM.display_admin_dashboard(request.user)}
    return render(request, 'old_pages/admin_dashboard.html', result)


# NOT READY
@authorized
def transactions(request):
    result = {
        'active_page': 'transactions'
    }

    auth_check = admin_check(request)
    if not auth_check:
        result['is_admin'] = True
    result = {**result, **MM.display_transacitons()}
    return render(request, 'old_pages/transactions.html', result)

@authorized
def admin_rewards(request):
    auth_check = admin_check(request)
    if auth_check:
        return redirect('/markets/')

    result = {
        'active_page': 'rewards',
        'is_admin': True
    }

    if request.method == 'POST':
        result = {**result, **MM.create_reward(request.POST)}

    result = {**result, **MM.display_rewards_investors(request.user)}

    return render(request, 'old_pages/admin_rewards.html', result)


@authorized
def test(request):
    a = MM.get_user_exchange(request.user).create_order('BTC_USDT', 1, 55, 'buy')
    return HttpResponse('OK')


def trade_list(request):
    return HttpResponse(json.dumps(MM.get_user_exchange(request.user).api('trade/list')))


def order_list(request):
    return HttpResponse(json.dumps(MM.get_user_exchange(request.user).api('order/list')))


def order_info(request):
    return HttpResponse(json.dumps(MM.get_user_exchange(request.user).api('order/info', {'order_id': int(request.GET['order_id'])})))


def order_new(request):
    # params = {"pair_id": 7, "amount": 0.0013624, "price": 32800, "type": "sell"}
    return HttpResponse(json.dumps(MM.get_user_exchange(request.user).api('order/new', {})))


def order_cancel(request):
    return HttpResponse(json.dumps(MM.get_user_exchange(request.user).api('order/cancel', {'order_id': int(request.GET['order_id'])})))


def transaction_list(request):
    return HttpResponse(json.dumps(MM.get_user_exchange(request.user).api('transaction/list')))


def orderbook_ticker(request):
    x = MM.get_user_exchange(request.user).api('orderbook/ticker', {}, 'GET', jsoned=False)
    return HttpResponse(json.dumps(x))


# API ENDPOINT @todo not ready. Integrate it with settings page.
def test_api_credentials():
    pass


def test_temp(request):
    #admin_user = User.objects.get(username='admin')
    #exchange = MM.get_user_exchange(admin_user, 'gateio')

    x = Week.objects.all()
    print(x)
    # response = exchange.api('/spot/orders/93162838961', 'currency_pair=MIMIR_USDT', 'DELETE')
    # print(response)
    #response = exchange.api('/spot/open_orders', '', 'GET')
    #print(response)
    #print("---")

    #exchange.cancel_all_orders()

    #order = Order.objects.get(eid='93162838961')
    #exchange.cancel_order(order)



    return HttpResponse('OK')

# NEW IMPLEMENTATIONS

def settings(request):
    response = login_check(request)
    if response:
        return response
    result = {
        'active_page': 'settings',
        'title': 'Settings'
    }

    auth_check = admin_check(request)
    if not auth_check:
        result['is_admin'] = True
        if 'trade-engine' in request.GET:
            MM.set_trade_engine_status(request.GET['trade-engine'])
        tt_status = MM.get_trade_engine_status()
        if tt_status:
            result['trade_engine_status'] = 'Active'
            result['trade_engine_status_style'] = 'success'
            result['trade_engine_btn'] = 'off'
        else:
            result['trade_engine_status'] = 'Passive'
            result['trade_engine_status_style'] = 'warning'
            result['trade_engine_btn'] = 'on'

    form = CustomPasswordChangeForm(request.user)
    result['form'] = form

    if request.method == 'POST':
        # API UPDATE
        if 'exchange-name' in request.POST:
            credential_check = MM.check_credentials(request.POST['exchange-name'], request.POST['api_private_key'], request.POST['api_public_key'])
            if credential_check:
                MM.delete_user_exchange(request.user, 'monetum')  # @todo make it dynamic
                UserMeta.update_user_meta(request.user, 'api_private_key', request.POST['api_private_key'])
                UserMeta.update_user_meta(request.user, 'api_public_key', request.POST['api_public_key'])
                UserMeta.update_user_meta(request.user, 'account_status', get_object_or_404(Status, name='ACTIVE'))
                result['success'] = 'API Credential added successfully. Please check your information below.'
            else:
                result['error'] = 'API Credential not added! Cannot connect exchange. Please check your information below.'
                #return render(request, 'pages/settings.html', result)
        elif 'old_password' in request.POST:
            form = CustomPasswordChangeForm(request.user, request.POST)
            if form.is_valid():
                form.save()
                result['success'] = 'Password updated successfully!'
            else:
                result['error'] = 'Password cannot changed!'

    result = {**result, **view_utils.get_all_exchange_credentials(request.user)}
    return render(request, 'pages/settings.html', result)


def markets(request):
    response = login_check(request)
    if response:
        return response
    result = {
        'active_page': 'markets',
        'title': 'Markets'
    }
    auth_check = admin_check(request)
    if not auth_check:
        result['is_admin'] = True

    if request.method == 'POST':
        result = {**result, **MM.create_reward(request.POST)}

    result['total_liquidity'] = MM.get_total_liquidity()
    result['number_of_bots'] = MM.get_number_of_bots()
    result['day_volume'] = MM.get_day_volume()
    result['weekly_yield'] = MM.get_weekly_yield()
    result['hourly_yield'] = MM.get_hourly_yield()
    result['monetum_pairs'] = MM.get_pairs('monetum')
    result['gateio_pairs'] = MM.get_pairs('gateio')
    result['monetum_currencies'] = MM.get_currencies('monetum')
    result['gateio_currencies'] = MM.get_currencies('gateio')

    result = {**result, **MM.get_all_pools()}

    return render(request, 'pages/markets.html', result)


def bot(request):
    response = login_check(request)
    if response:
        return response
    result = {
        'active_page': 'bot',
        'title': 'Bot'
    }
    response = is_user_valid(request)
    if response:
        return response
    auth_check = admin_check(request)
    if not auth_check:
        result['is_admin'] = True

    if request.method == 'POST':
        result = {**result, **MM.create_investment(request.user, request.POST)}
        pool_id = request.POST['reward_id']
        if 'error' not in result.keys():
            result['success'] = 'Invested successfully!'

    else:
        if 'pool_id' in request.GET.keys():
            pool_id = request.GET['pool_id']
            if 'cancel' in request.GET.keys():
                result = {**result, **MM.cancel_investment(request.user, pool_id)}
        else:
            return redirect('home')
    print(pool_id)
    reward = Reward.objects.get(id=pool_id)
    reward.week = MM.get_week_of_reward(reward.id)
    reward.weekly_reward = MM.get_reward_of_week(reward.id)
    reward.weekly_yield = MM.get_weekly_yield(reward.id)
    reward.hourly_yield = MM.get_hourly_yield(reward.id)
    is_user_invested = reward.is_user_invested(request.user)
    any_investments = Investment.objects.filter(user=request.user)
    if len(any_investments) > 0:
        result['wallet_address'] = Investment.objects.filter(user=request.user)[0].wallet_address
    result['is_user_invested'] = is_user_invested
    result['reward'] = reward
    result['pool'] = reward
    result = {**result, **MM.get_user_balance_for_reward(request.user, reward)}
    result['current_share_of_pool'] = MM.get_current_share_of_pool(reward, result['balance'].total_value)
    if is_user_invested:
        result = {**result, **MM.get_rewardsharings(reward.id, request.user)}
        result = {**result, **MM.get_orders(reward.id, request.user)}
    return render(request, 'pages/bot.html', result)


def cancel_order(request):
    keys = request.GET.keys()
    if 'order_eid' in keys and 'user_id' in keys and 'order_id' in keys:
        result = MM.api_cancel_order(request.GET['user_id'], request.GET['order_eid'], request.GET['order_id'])
        return HttpResponse(result)
    else:
        return HttpResponse('{"error":false, "message":"Unauthorized!"}')


def admin_panel(request):
    response = login_check(request)
    result = {'active_page': 'admin_dashboard', 'title': 'Admin Panel'}
    if response:
        return response
    auth_check = admin_check(request)
    if auth_check:
        return auth_check
    else:
        result['is_admin'] = True
    result = {**result, **MM.display_admin_dashboard(request.user)}
    return render(request, 'pages/admin_panel.html', result)


# =========================
#
# STATIC PAGES
#
# =========================


def wiki(request):
    return render(request, 'static_pages/wiki.html')


def terms_of_use(request):
    return render(request, 'static_pages/terms_of_use.html')


def privacy_policy(request):
    return render(request, 'static_pages/privacy_policy.html')


def contact_us(request):
    return render(request, 'static_pages/contact_us.html')


def faqs(request):
    return render(request, 'static_pages/faqs.html')


def how_to_start(request):
    return render(request, 'static_pages/how_to_start.html')
