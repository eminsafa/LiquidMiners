"""MarketMaker URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.contrib.auth.views import LoginView
from . import views


urlpatterns = [

    path('admin/dashboard/', views.admin_dashboard, name='admin_dashboard'),
    path('admin/transactions/', views.transactions, name='transactions'),
    path('admin/liquidity_pools/', views.admin_rewards, name='admin_rewards'),
    path('test/order/list', views.order_list, name='order_list'),
    path('test/', views.test_temp, name='test_temp'),


    path('', views.home, name='index'),
    path('home/', views.home, name='home'),
    path('markets/', views.markets, name='markets'),
    path('bot/', views.bot, name='bot'),
    path('settings/', views.settings, name='settings'),
    path('login/', LoginView.as_view(template_name='pages/login.html'), name="login"),
    path('register/', views.register, name='register'),
    path('admin/panel', views.admin_panel, name='admin_panel'),
    path('logout/', views.user_logout, name='logout'),

    path('api/cancel_order', views.cancel_order, name='cancel_order'),
    path('admin/', admin.site.urls, name='admin'),

    # Static Pages
    path('wiki', views.wiki, name='wiki'),
    path('terms_of_use', views.terms_of_use, name='terms_of_use'),
    path('privacy_policy', views.privacy_policy, name='privacy_policy'),
    path('contact_us', views.contact_us, name='contact_us'),
    path('faqs', views.faqs, name='faqs'),
    path('how_to_start', views.how_to_start, name='how_to_start'),

    path('initialize', views.initialize),
]


