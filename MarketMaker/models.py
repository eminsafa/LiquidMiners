from django.db import models
from django.contrib.auth.models import AbstractUser
import datetime


class User(AbstractUser):
    email = models.EmailField('email', unique=True)


class Status(models.Model):

    # Status Info
    name = models.CharField(null=False, unique=True, max_length=32)
    style = models.CharField(null=True, max_length=64)


class Week(models.Model):

    # Week Info
    start = models.DateTimeField(null=True)
    end = models.DateTimeField(null=True)

    @staticmethod
    def get(dt):
        return Week.objects.get(start__lte=dt, end__gte=dt)


class Currency(models.Model):

    # Form Data
    eid = models.CharField(max_length=16, null=False, default=1)  # ID on Exchange
    symbol = models.CharField(max_length=16)
    name = models.CharField(max_length=32)
    icon = models.CharField(max_length=16, null=True, default='none')
    exchange = models.CharField(max_length=16, null=True)


class Pair(models.Model):

    # Form Data
    eid = models.CharField(max_length=16, null=False, default=1)  # ID on Exchange
    name = models.CharField(max_length=32, null=False, default='<PAIR>')
    buy_side = models.ForeignKey(Currency, on_delete=models.CASCADE, null=True, related_name='buy_side')
    sell_side = models.ForeignKey(Currency, on_delete=models.CASCADE, null=True, related_name='sell_side')
    exchange = models.CharField(max_length=16, null=True)


class CampaignWeekPayment(models.Model):
    amount = models.DecimalField(max_digits=10, decimal_places=2)


class Reward(models.Model):

    # Form Data
    pair = models.ForeignKey(Pair, on_delete=models.CASCADE, null=True)
    name = models.CharField(max_length=32, null=True)
    amount = models.FloatField(null=True)
    currency = models.ForeignKey(Currency, on_delete=models.CASCADE, null=True)
    start_date = models.DateField(null=False, default=datetime.date.today)
    end_date = models.DateField(null=False, default=datetime.date.today)
    status = models.ForeignKey(Status, on_delete=models.CASCADE, null=True)
    exchange = models.CharField(max_length=32, null=True, default='monetum')
    campaign_detail = models.TextField(max_length=150, null=True)
    rules = models.TextField(max_length=50, null=True)
    reward_detail = models.TextField(max_length=50, null=True)
    campaign_duration = models.IntegerField(null=True)
    weekly_amount = models.ManyToManyField('CampaignWeekPayment')

    def get_investments_total(self):
        total = 0.0
        investments = Investment.objects.filter(reward=self)
        for i in investments:
            total += i.total_value
        return float(total)

    def get_weeks(self):
        start_week = Week.get(self.start_date).id
        end_week = Week.get(self.end_date).id
        weeks = []
        for w in range(start_week, end_week + 1):
            weeks.append(w)
        return weeks

    def get_spent_time(self, week):
        investments = Investment.objects.filter(self)
        week_start = week.start.timestamp()
        week_end = week.end.timestamp()
        total = 0
        for i in investments:
            if week_start > i.created_at.timestamp():
                relative_time = week_start
            else:
                relative_time = i.created_at.timestamp()
            total += week_end - relative_time
        return float(round(total / 1000.0, 2))

    def get_user_spent_time(self, week, user):
        i = Investment.objects.get(reward=self, user=user, status__name='ACTIVE')
        week_start = week.start.timestamp()
        week_end = week.end.timestamp()
        if week_start > i.created_at.timestamp():
            relative_time = week_start
        else:
            relative_time = i.created_at.timestamp()
        return round((week_end - relative_time) / 1000.0, 2)

    def get_order_count(self):
        investments = Investment.objects.filter(reward=self)
        number_of_orders = 0
        for i in investments:
            number_of_orders += len(Order.objects.filter(investment=i))
        return number_of_orders

    def is_user_invested(self, user):
        investments = Investment.objects.filter(user=user, reward=self, status__name='ACTIVE')
        if len(investments) > 0:
            return True
        else:
            return False


class Investment(models.Model):

    # Form Data
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    reward = models.ForeignKey(Reward, on_delete=models.CASCADE, null=True)
    minimum_spread = models.FloatField(null=False, default=0)
    maximum_spread = models.FloatField(null=False, default=0)
    wallet_address = models.TextField(null=False, default=0)
    coin_amount = models.FloatField(null=False, default=0)
    fiat_amount = models.FloatField(null=True, default=0)
    safety_net = models.BooleanField(null=True, default=False)

    # Trade Engine Parameters
    total_value = models.FloatField(null=True)
    status = models.ForeignKey(Status, on_delete=models.CASCADE, null=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)
    buy_weight = models.IntegerField(null=True, default=1)
    sell_weight = models.IntegerField(null=True, default=1)

    # pair is in Reward Model

    @staticmethod
    def get_total_all():
        total = 0.0
        investments = Investment.objects.all()
        for i in investments:
            total += i.total_value
        return float(total)

    def get_total_earnings(self):
        reward_sharings = RewardSharing.objects.filter(investment=self)
        total = 0.0
        for r in reward_sharings:
            total += r.amount
        return float(total)


class Order(models.Model):

    # Form Data
    investment = models.ForeignKey(Investment, on_delete=models.CASCADE, null=True)
    eid = models.CharField(max_length=32, null=False, default=1)
    amount = models.FloatField(null=True)
    price = models.FloatField(null=True)
    side = models.CharField(max_length=9, default='BUY', null=True)
    status = models.ForeignKey(Status, on_delete=models.CASCADE, null=True)
    trigger_at = models.DateTimeField(auto_now_add=True, null=True)  # @todo 50 saniye sonrası
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)
    temp = ''
    # reward is in Investment Model
    # pair is in Reward Model that in Investment

    def set_status(self, name):
        self.status = Status.objects.get(name=name)
        self.save()

    @property
    def amount_rounded(self):
        return round(self.amount, 2)

    @property
    def price_rounded(self):
        return round(self.price, 4)


class RewardSharing(models.Model):

    # Sharing
    reward = models.ForeignKey(Reward, on_delete=models.CASCADE, null=True)
    investment = models.ForeignKey(Investment, on_delete=models.CASCADE, null=True)
    ratio = models.FloatField(null=False, default=0.0)
    amount = models.FloatField(null=False, default=0.0)
    week = models.ForeignKey(Week, on_delete=models.CASCADE, null=True)
    # currency is in Reward Model
    # user is in Investment Model


class Transaction(models.Model):

    reward = models.ForeignKey(Reward, on_delete=models.CASCADE, null=True)
    week = models.ForeignKey(Week, on_delete=models.CASCADE, null=True)
    status = models.ForeignKey(Status, on_delete=models.CASCADE, null=True)

    @staticmethod
    def get_status(reward, week):
        transaction_status = Transaction.objects.filter(reward=reward, week=week)
        if len(transaction_status) == 0:
            status = Status.objects.get(name='NOT PROCESSED')
            t = Transaction(reward=reward, week=week, status=status)
            t.save()
            return t.status
        elif len(transaction_status) == 1:
            return transaction_status[0].status
        else:
            Log('error', 'There are multiple Transaction! Rew: ' + str(reward.id) + ' Week: ' + str(week.id)).save()
            return transaction_status[0].status


class Log(models.Model):

    # Log
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    timestamp = models.DateTimeField(auto_now_add=True, null=True)
    type = models.CharField(max_length=16, default='INFO', null=True)
    message = models.TextField()

    @staticmethod
    def test():
        print('OK')

    @staticmethod
    def on(message, type='INFO', user=None):
        try:
            Log(user=user, type=type, message=message).save()
        except Exception as e:
            print("\033[0;31m" + "<<< LOG SAVING ERROR >>>" + "\033[0m")
            print("\033[0;31m" + f"{type(e).__name__} at line {e.__traceback__.tb_lineno} of {__file__}: {e}" + "\033[0m")
            return False
        return True



class APILog(models.Model):

    # Log
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    timestamp = models.DateTimeField(auto_now_add=True, null=True)
    endpoint = models.CharField(max_length=128, null=True)
    params = models.TextField()
    response = models.TextField()


class UserMeta(models.Model):

    # User Meta
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    type = models.CharField(max_length=32)
    meta = models.TextField()

    @staticmethod
    def get_api_credentials(user, exchange_name='monetum'):
        result = {}
        keys = UserMeta.objects.filter(user=user, type=exchange_name+'_private_key')
        if len(keys) > 0:
            try:
                result['api_private_key'] = UserMeta.objects.get(user=user, type=exchange_name+'_private_key').meta
                result['api_public_key'] = UserMeta.objects.get(user=user, type=exchange_name+'_public_key').meta
            except:
                Log(user=user, type='error', message='User have not API or have multiple API').save()
                result['error'] = 'You have not defined API credentials yet. Please add API credentials on the Settings page before creating new investment.'
        else:
            result['error'] = 'You have not defined API credentials yet. Please add API credentials on the Settings page before creating new investment.'
        return result

    @staticmethod
    def update_user_meta(user, key, value):
        keys = UserMeta.objects.filter(user=user, type=key)
        for i in keys:
            i.delete()
        new_user_meta = UserMeta(user=user, type=key, meta=value)
        new_user_meta.save()

class File(models.Model):

    # Files
    name = models.CharField(max_length=64, default='Unnamed File', null=False)
    description = models.TextField()
    path = models.TextField()
    # 0: ADMIN, 1: SPECIFIC CUSTOMER, 2: PUBLIC
    permission = models.IntegerField()
    # IF permission == 1:  ADMIN AND CUSTOMER can access
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)


class Configuration(models.Model):

    # System Configuration
    type = models.CharField(max_length=32)
    meta = models.TextField()


class Price(models.Model):

    pair = models.ForeignKey(Pair, on_delete=models.CASCADE, null=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    price = models.FloatField(null=False, default=0)


class Cache(models.Model):

    class_name = models.CharField(max_length=64, default='MarketMaker', null=False)
    method_name = models.CharField(max_length=64, default='main', null=False)
    params = models.TextField()
    result_type = models.CharField(max_length=16)
    result = models.TextField()
    last_update = models.DateTimeField(auto_now_add=True, null=True)

