/*

=========================================================
* MarketMaker v1.0
=========================================================

* Copyright 2021

* Developed By Safa Tok (eminsafa [at] yaani.com)

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the

* Software. Please contact us to request a removal. Contact us if you want to remove it.

*/

// =====    Configuration
var conf = {
    activePage: '',
    pages: [
        'admin_dashboard',
        'dashboard',
        'home',
        'investments',
        'new_investment',
        'register',
        'rewards',
        'settings',
        'transactions',
        'reward_details_public'
    ],
    data: {
    }
};

// =====    API Fetcher
async function fetchData(url, notjson=false, contentType='application/json') {

    try {
        const response = await fetch(url, {
            method: 'GET',
            mode: 'cors',
            cache: 'no-cache',
            credentials: 'same-origin',
            headers: {
                'Content-Type': contentType,
                'Access-Control-Allow-Origin': '*'
            },
            redirect: 'follow',
            referrerPolicy: 'no-referrer'
        });
        if(notjson){
            console.log(response);
            return response;
        }else{
            return response.json();
        }
    } catch (e) {
        console.error('ERROR:' + e);
    }
}

// =====    Cookie Fetcher
function getCookie(name) {
    name = name + '=';
    var decodedCookie = decodeURIComponent(document.cookie);
    var cookies = decodedCookie.split(';');
    for (var i = 0; i < cookies.length; i++) {
        var cookie = cookies[i].trim();
        if (cookie.indexOf(name) == 0) {
            return cookie.substring(name.length, cookie.length);
        }
    }
}

// =====    Data Export
function dataExport(page){

    if(page === 'balances'){
        var exportData = [
            ['Currency', 'Free ', 'Used', 'Total', 'Free ($USD)', 'Used ($USD)', 'Total ($USD)']
        ];
        for (let i = 0; i < conf.data.balances.length; i++) {
            var tempArr = []
            var d = conf.data.balances[i];
            tempArr.push(d.currency);
            tempArr.push(d.base_free);
            tempArr.push(d.base_used);
            tempArr.push(d.base_total);
            tempArr.push(d.usd_free);
            tempArr.push(d.usd_used);
            tempArr.push(d.usd_total);
            exportData.push(tempArr);
        }
    }

    let csvContent = "data:text/csv;charset=utf-8,"
        + exportData.map(e => e.join(",")).join("\n");
    var encodedUri = encodeURI(csvContent);
    var link = document.createElement("a");
    link.setAttribute("href", encodedUri);
    link.setAttribute("download", "PulseBot_Balances_"+getDate()+".csv");
    document.body.appendChild(link);
    link.click();
}

// =====    Date & Time String
function getDate(){
    const d = new Date();
    return d.getFullYear().toString()+'-'
        +('0' + d.getMonth()).slice(-2)+'-'
        +('0' + d.getDay()).slice(-2)+'_'
        +('0' + d.getHours()).slice(-2)+'.'
        +('0' + d.getMinutes()).slice(-2)+'.'
        +('0' + d.getSeconds()).slice(-2);
}

// =====    Page Detect
function setPageName(){
    const activePage = document.getElementById('active-page-data-container');
    var pageName = activePage.getAttribute('data-active-page');
    conf.activePage = pageName;
    return pageName;
    // @todo may be add page name controller/checker
}

// =====    Time Countdown
function timeCountdown(element, countDownDate) {

    var now = new Date().getTime();
    var distance = countDownDate - now;
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    element.innerHTML = days + "d " + hours + "h " + minutes + "m ";
    if (distance < 0) {
    element.innerHTML = "<span class='text-danger'>Expired</span>";
    }

}

// =====    Time Countdown Cron
function timeCountdownCron(element){

    var date = element.getAttribute('data-date');
    var countDownDate = new Date(date+" 23:59:00").getTime();
    timeCountdown(element, countDownDate);
    setInterval( timeCountdown(element, countDownDate), 6000);

}

// =====    Export Table of Transactions
function exportTransactions(tableID, week){

    var table = document.getElementById(tableID);

    var title = 'Transactions_Week_'+week+'_';
    var exportData = [
        ['ID', 'E-mail', 'Amount', 'Currency', 'Pair', 'Exchange', 'Wallet Address']
    ];
    var row = '';
    var tempArr = '';
    for (let i = 0; i < table.rows.length; i++) {
        tempArr = [];
        row = table.rows[i];
        tempArr.push(row.getAttribute('data-t-id'));
        tempArr.push(row.getAttribute('data-t-email'));
        tempArr.push(row.getAttribute('data-t-amount'));
        tempArr.push(row.getAttribute('data-t-currency'));
        tempArr.push(row.getAttribute('data-t-pair'));
        tempArr.push(row.getAttribute('data-t-exchange'));
        tempArr.push(row.getAttribute('data-t-wallet'));
        exportData.push(tempArr);
    }


    let csvContent = "data:text/csv;charset=utf-8,"
        + exportData.map(e => e.join(",")).join("\n");
    var encodedUri = encodeURI(csvContent);
    var link = document.createElement("a");
    link.setAttribute("href", encodedUri);
    link.setAttribute("download", title+"_"+getDate()+".csv");
    document.body.appendChild(link);
    link.click();

}

function updateTransactionStatus(week_id, reward_id){
    var status_id = document.getElementById('transaction-update-selector').value;
    if(status_id !== "0"){
        var selected_value = '';
        switch (status_id) {
            case '6':
                selected_value = 'PROCESSED';
                break;
            case '7':
                selected_value = 'PROCESSING';
                break;
            case '8':
                selected_value = 'NOT PROCESSED';
                break;
        }
        var r = confirm("Are you sure to update status to "+selected_value+" ?");
        if (r == true) {
          window.location.href = window.location.origin+'/admin/reward-details/?action=update&reward_id='+reward_id+'&week_id='+week_id+'&status_id='+status_id;
        }
    }

}

async function apiCall(path){
    try {
        const response = await fetch(path, {
            method: 'GET',
            mode: 'cors',
            cache: 'no-cache',
            credentials: 'same-origin',
            headers: {
                'Content-Type': 'application/json, text/plain',
                'Access-Control-Allow-Origin': '*'
            },
            redirect: 'follow',
            referrerPolicy: 'no-referrer'
        });
        return response.json();
    }catch (e) {
        console.error('ERROR: '+e);
    }
}

async function cancelOrder(path, eid){
    var result = await apiCall(path);
    console.log(result);
    if (result.error === true){
        newNotification('red', result.message);
    }else{
        newNotification('green', result.message);
        var cancelButton = document.getElementById('cancel-button-'+eid);
        cancelButton.remove();
        var statusBox = document.getElementById('order-status-'+eid);
        statusBox.innerText = 'CANCELLED';
        statusBox.className = 'btn btn-sm btn-danger';
    }
}

function inputDisableToggle(elementId){
    var element = document.getElementById(elementId);
    var disableStatus = element.getAttribute('disabled');
    if (disableStatus){
        element.removeAttribute('disabled');
    }else{
        element.setAttribute('disabled', true);
    }
}


function startTheBot() {
    document.getElementById("theBotForm").submit();
}


//      Throw New Notification
function newNotification(color, message){
    const notyf = new Notyf({
        position: {
            x: 'right',
            y: 'bottom',
        },
        duration: 2700,
        types: [
            {
                type: 'error',
                background: color,
                icon: {
                    className: 'fas fa-exclamation-circle',
                    tagName: 'span',
                    color: '#fff'
                },
                dismissible: false
            }
        ]
    });
    notyf.open({
        type: 'error',
        message: message
    });

}

// =====    ====   PAGES

// =====    PAGE: New Investment
function pageNewInvestment(){
    const countDownElement = document.getElementById('reward-count-down');
    timeCountdownCron(countDownElement);
}


// =====    Initializer
async function initializer(){
    setPageName();
    console.log(conf.activePage);

    if(conf.activePage === 'new_investment'){
        console.log('New Investment');
        pageNewInvestment();
    }else if(conf.activePage === 'reward_details_public'){
        console.log('<<OK>>');
        pageNewInvestment();
    }
    const countDownElement = document.getElementById('reward-count-down');
    if(![undefined, null].includes(countDownElement)){
        timeCountdownCron(countDownElement);
    }

}


// =====    Validator
function newInvestmentValidation(){
    var form = document.getElementById('new-investment-form');

    if (!form.checkValidity()) {
      console.log('not validated');
    }
    form.classList.add('was-validated');
}
console.log('READY');
// =====    Initialize
/*
$(document).ready(function () {

    initializer();

});
*/


